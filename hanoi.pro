/*****************************************************************************

		Copyright (c) My Company

 Project:  HANOI
 FileName: HANOI.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "hanoi.inc"

domains
	ficha=integer
	columna=ficha*
	estado=e(columna,columna,columna)
	lista=estado*
	limite=integer

predicates

	
	mueve(estado,estado)
	move(columna,columna,columna,columna)
	izqd_a_medio(columna,columna,columna,columna)
	medio_a_izqd(columna,columna,columna,columna)
	izqd_a_dcha(columna,columna,columna,columna)
	dcha_a_izqd(columna,columna,columna,columna)
	dcha_a_medio(columna,columna,columna,columna)
	medio_a_dcha(columna,columna,columna,columna)
	
  	inseguro(columna,columna)
  	
  
  	miembro(estado,lista)
  	resuelve(lista,estado,limite,limite) /* Lista de estados, Estado destino */
  	escribe(lista)
  	mejorsol(integer)
	
 

clauses
	/*Left to Middle*/
	mueve( e(LB,MB,SR), e(LA,MA,SR) ):-
		izqd_a_medio(LB,LA,MB,MA).
		
	/*Medium to Left*/	
	mueve( e(LB,MB,SR), e(LA,MA,SR) ):-
		medio_a_izqd(LB,LA,MB,MA).
			
	/*Medium to Right*/
	mueve( e(SL,MB,RB), e(SL,MA,RA) ):-
		medio_a_dcha(MB,MA,RB,RA).
	
	/*Right to Medium*/
	mueve( e(SL,MB,RB), e(SL,MA,RA) ):-
		dcha_a_medio(MB,MA,RB,RA).
		
	/*Left to Right*/	
	mueve( e(LB,SM,RB), e(LA,SM,RA) ):-
		izqd_a_dcha(LB,LA,RB,RA).
	
	/*Right to Left*/
	mueve( e(LB,SM,RB), e(LA,SM,RA) ):-
		dcha_a_izqd(LB,LA,RB,RA).
		
	
	
	/*LEFT AND MEDIUM*/	
	izqd_a_medio(LB,LA,MB,MA):-	
			not(inseguro(LB,MB)),
			move(LB,LA,MB,MA).
		
	medio_a_izqd(LB,LA,MB,MA):-	
			not(inseguro(MB,LB)),
			move(MB,MA,LB,LA).
	
	/*MEDIUM AND RIGHT*/		
	medio_a_dcha(MB,MA,RB,RA):-
			not(inseguro(MB,RB)),
			move(MB,MA,RB,RA).
	
	dcha_a_medio(MB,MA,RB,RA):-
			not(inseguro(RB,MB)),
			move(RB,RA,MB,MA).
	
	/*LEFT AND RIGHT*/
	izqd_a_dcha(LB,LA,RB,RA):-
			not(inseguro(LB,RB)),
			move(LB,LA,RB,RA).
			
	dcha_a_izqd(LB,LA,RB,RA):-
			not(inseguro(RB,LB)),
			move(RB,RA,LB,LA).
			
	
	move(AB,AA,BB,BA):-
		AB = [H|T],
		AA = T,
		BA = [H|BB].
	
	inseguro(A,B):-
		A = [AH|_],
		B = [BH|_],
		AH > BH.		
	

        /*Estados repetidos */
        miembro(E,[E|_]).
        miembro(E,[_|T]):-
        	miembro(E,T).
        	
        /*Resolucion de algoritmo */
        resuelve(Lista,Destino,_,_):-
        	Lista=[H|T],
        	Destino=H,
        	escribe(Lista).
        
        resuelve(Lista,Destino,Lim_ant,Limite):-
        	Lista=[H|T],
        	not(miembro(H,T)),
        	mueve(H,Hfinal),
        	Nlista=[Hfinal|Lista],
        	Nue_Lim=Lim_ant+1,
        	Nue_Lim<=Limite,
        	resuelve(Nlista,Destino,Nue_Lim,Limite).
        	
        /*Escritura de la lista */
        escribe([]).
        escribe([H|T]):-
        	escribe(T),
        	write(H,'\n').
        	
        mejorsol(Lim_ini):-
        	resuelve( [e([1,2,3],[],[])],e([],[],[1,2,3]) ,0,7).
        	
        mejorsol(Lim_ini):-
        	Nue_lim=Lim_ini+1,
        	mejorsol(Nue_lim).

goal


/*resuelve([estado(e,e,e,e)],estado(o,o,o,o),1,8).*/
	mejorsol(1).